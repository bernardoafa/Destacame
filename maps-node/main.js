var express = require('express');
var app = express();
var server = require('http').Server(app);
var io = require('socket.io')(server);
var port = 8080;
var marker = [];

app.use(express.static('public'));

app.get('/', function(req, res) {
  res.status(200).send("Node!!");
});

io.on('connection', function(socket) {
  socket.on('new-marker', function(data) {
    socket.broadcast.emit('marker', data);
  });
});

server.listen(port, function() {
  console.log("Servidor corriendo en http://localhost:"+port);
});
