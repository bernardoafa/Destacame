# Crear Base de datos (Postgresql)

- createdb -O postgres django_dev -E utf-8

# Pasos para ejecutar el proyecto (DEV)

- virtualenv env
- source env/bin/activate
- cd maps
- pip install -r requirements.txt
- ./manage.py makemigrations location
- ./manage.py migrate
- python manage.py createsuperuser
- ./manage.py type_db
- ./manage.py runserver

# Ejecutar node
- cd ../maps-node/
- npm install
- node main.js