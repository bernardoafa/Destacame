from django.core.management.base import BaseCommand
from location.models import Type

class Command(BaseCommand):
    args = '<foo bar ...>'
    help = 'our help string comes here'

    def _create_Types(self):
        tlisp = Type(name='Calle',abrv='C')
        tlisp.save()

        tlisp = Type(name='Edificio',abrv='E')
        tlisp.save()

        tlisp = Type(name='Avenida',abrv='A')
        tlisp.save()

        tlisp = Type(name='Local',abrv='L')
        tlisp.save()

        tlisp = Type(name='Metro',abrv='M')
        tlisp.save()

        tlisp = Type(name='Supermercado',abrv='S')
        tlisp.save()


    def handle(self, *args, **options):
        self._create_Types()