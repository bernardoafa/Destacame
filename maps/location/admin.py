from django.contrib import admin

# Register your models here.

from .models import Location


class LocationAdmin(admin.ModelAdmin):
    list_display = ('name','address','type_id','lng','lat')
    list_display = ('name','address','type_name','status')
    list_filter = ('created','type__name')
    search_fields = ['name','address','type__name']

    def make_activar(modeladmin, request, queryset):
        rows_updated = queryset.update(status='A')
    make_activar.short_description = "Mostrar"

    def make_desactivar(modeladmin, request, queryset):
        queryset.update(status='I')
    make_desactivar.short_description = "Ocultar"

    actions = [make_activar,make_desactivar]

admin.site.register(Location,LocationAdmin)