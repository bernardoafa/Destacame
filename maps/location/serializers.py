from django.forms import widgets
from rest_framework import serializers
from location.models import Location, Type
    
class LocationSerializer(serializers.ModelSerializer):
    type_abrv = serializers.StringRelatedField()    
    class Meta:
        model = Location
        fields = ('id', 'name', 'address', 'lng', 'lat','type','type_abrv','status')


class TypeSerializer(serializers.ModelSerializer):
    class Meta:
        model = Type
        fields = ('id', 'name','abrv')