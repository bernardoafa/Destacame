from location.models import Location, Type
from location.serializers import LocationSerializer, TypeSerializer
from rest_framework import generics

class LocationList(generics.ListCreateAPIView):
    #queryset = Location.objects.all()
    queryset = Location.objects.filter(status__exact='A')
    serializer_class = LocationSerializer
    
class LocationDetail(generics.RetrieveUpdateDestroyAPIView):
    #queryset = Location.objects.all()
    queryset = Location.objects.filter(status__exact='A')
    serializer_class = LocationSerializer

class TypeList(generics.ListCreateAPIView):
    queryset = Type.objects.all()
    serializer_class = TypeSerializer
    
class TypeDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Type.objects.all()
    serializer_class = TypeSerializer
