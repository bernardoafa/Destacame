from django.conf.urls import include
from django.conf.urls import url
from rest_framework.urlpatterns import format_suffix_patterns
from location import views

urlpatterns = [
    url(r'^location/$', views.LocationList.as_view()),
    url(r'^location/(?P<pk>[0-9]+)/$', views.LocationDetail.as_view()),
    url(r'^type/$', views.TypeList.as_view()),
    url(r'^type/(?P<pk>[0-9]+)/$', views.TypeDetail.as_view()),
]